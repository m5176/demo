

--
-- 업체 정보 테이블
CREATE TABLE `CompanyList` (
  `seq` bigint(20) NOT NULL AUTO_INCREMENT,
  `companyname` varchar(64) NOT NULL,
  `status` char(1) DEFAULT 'Y',
  `regdate` timestamp NOT NULL DEFAULT current_timestamp(),
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Team 정보 테이블
CREATE TABLE `TeamList` (
  `teamcode` char(4) NOT NULL,
  `teamname` varchar(32) DEFAULT NULL,
  `status` char(1) DEFAULT 'Y',
  `regdate` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`teamcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 사용자 정보 테이블
CREATE TABLE `UserList` (
  `seq` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) NOT NULL,
  `password` varchar(100) NOT NULL,
  `username` varchar(32) DEFAULT NULL,
  `usergrade` enum('Root', 'Admin', 'Manager', 'Agent') NOT NULL,
  `teamcode` char(4) DEFAULT NULL,
  `regdate` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;





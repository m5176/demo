package org.asterisk.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberDto {

    private Long seq;
    private String userid;
    private String password;
    private String username;

}

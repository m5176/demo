package org.asterisk.demo.controller;


import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.asterisk.demo.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@Slf4j
public class FileController {

    @Autowired
    private FileService fileService;

    @GetMapping("/file/fileMain")
    public String fileMain(){
        return "upload";
    }

    @PostMapping("/file/upload")
    @ApiOperation(value = "파일 업로드", notes = "")
    public String uploadFile(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes)throws Exception{

        log.info(file.getOriginalFilename());
        if( file == null || file.isEmpty() ){
            throw new Exception("MultipartFile is NULL");
        }
        fileService.uploadFile(file);
        //redirectAttributes.addFlashAttribute("message", "You successfully updateed : " + file.getOriginalFilename() + " !");

        return "redirect:/";
    }

}

package org.asterisk.demo.controller;

import org.asterisk.demo.dto.MemberDto;
import org.asterisk.demo.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MemberController {

    @Autowired
    MemberService memberService;

    @GetMapping("/member/memberMain")
    public String memberMain(){
        return "Member";
    }

    @PostMapping("/member/addMember")
    public String addMember(MemberDto member, Model model) {

        System.out.println("id : " + member.getUserid()
                + " / pw : " + member.getPassword()
                + " / name : " + member.getUsername());

        int insert = memberService.addMember(member);

        model.addAttribute("insert", insert);
        model.addAttribute("userid", member.getUserid());
        return "addMember";
    }

    /*@PostMapping("/member/getMember")
    public String getMember(Member member){

        Member m = new Member();
        m = memberRepository.
        return "GetMember";
    }*/
}

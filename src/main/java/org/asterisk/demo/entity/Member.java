package org.asterisk.demo.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name="member")
@ToString
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Member {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long seq;

    @Column(length=32, nullable=false)
    private String userid;

    @Column(length=100, nullable=false)
    private String password;

    @Column(length=32, nullable=true)
    private String username;
}

package org.asterisk.demo.service;

import org.asterisk.demo.dto.MemberDto;
import org.asterisk.demo.entity.Member;
import org.asterisk.demo.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MemberService {

    @Autowired
    MemberRepository memberRepository;

    public int addMember(MemberDto member) {

        int insert = -1;
        try{
            Member m = Member.builder()
                    .userid(member.getUserid())
                    .username(member.getUsername())
                    .password(member.getPassword())
                    .build();

            memberRepository.save(m);

        }catch(Exception e){
            e.printStackTrace();
        }finally {
            insert = 1;
        }

        return insert;
    }




}

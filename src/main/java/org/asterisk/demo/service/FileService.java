package org.asterisk.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.UUID;

@Service
@Slf4j
public class FileService {

    public FileService() {


    }

    public void uploadFile(MultipartFile file )throws Exception{
        log.info(file.getOriginalFilename());

        String rootPath = System.getProperty("user.dir") + "\\src\\main\\resources\\static";
        log.info(rootPath);

        UUID uuid = UUID.randomUUID();
        String postfix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1, file.getOriginalFilename().length() );
        log.info(" File Name : " + uuid.toString() + "." + postfix);

        String fileName = uuid + "." + postfix;
        File saved = new File(rootPath, fileName);
        file.transferTo(saved);
    }


}
